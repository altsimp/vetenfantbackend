package vetenfant.backend.vetenfant.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTests {
    @Autowired
    MockMvc mvc;

    @Test
    void testAll() throws Exception {
        mvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[?(@.accountName == null)]").isEmpty())
                .andExpect(jsonPath("$[?(@.password == null)]").isEmpty())
                .andExpect(jsonPath("$[?(@.email == null)]").isEmpty());
    }

    @Test
    void testAdd() throws Exception {
        mvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON) // On fait une requête post en envoyant du JSON et en créant
                                                         // celui ci en dur dans le test (comme on ferait avec postman
                                                         // ou thunder client)
                .content(
                        """
                                {
                                    "accountName":"john",
                                    "email":"test@test.com",
                                    "password":"1234",
                                    "phoneNumber": "0666778899",
                                    "name": "name",
                                    "firstName": "firstname",
                                    "description": "Ma premiere description",
                                    "profileImg": "https://media.licdn.com/media/AAYQAQSOAAgAAQAAAAAAAB-zrMZEDXI2T62PSuT6kpB6qg.png",
                                    "evaluation": 4.0,
                                    "role":"ROLE_USER"
                                }
                        """))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber()); // Ici $ représente l'objet racine JSON renvoyé par l'API, et
                                                         // on vérifie si l'id existe bien en number dessus
    }

    @Test
    void testAddValidationError() throws Exception {
        mvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {

                            "accountName:"Sarah"
                        }
                        """))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testById() throws Exception {
        mvc.perform(get("/api/user/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasKey("username")))
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void testByIdNotFound() throws Exception {
        mvc.perform(get("/api/user/100"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testUpdate() throws Exception {
        mvc.perform(patch("/api/user/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "accountName":"Sam"
                        }
                        """))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accountName").value("Sam"));
    }

    @Test
    void testDelete() throws Exception {
        mvc.perform(delete("/api/user/4"))
                .andExpect(status().isNoContent());
    }

    @Test
    void testDeleteNotFound() throws Exception {
        mvc.perform(delete("/api/user/16"))
                .andExpect(status().isNotFound());
    }

}
