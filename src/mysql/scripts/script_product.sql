-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
INSERT INTO product(name, gender, price, size, state, description, isSold, season, id_user, id_category) VALUES
('Veste', 'BOY', 10, 1, 'VERY_GOOD_CONDITION','Veste', 'SPRING', false, 1, 1),
('Pantalon', 'BOY', 15, 9, 'STATISFACTORY','Pantalon','SUMMER', false, 2, 2),
('T-shirt', 'GIRL', 5, 48, 'NEW_WITHOUT_TAGS', 'T-shirt', 'AUTUMN', false, 3, 3),
('Veste', 'UNISEX', 1, 120, 'NEW_WITHOUT_TAGS', 'Veste', false,'WINTER', 1, 1),
('Veste', 'BOY', 10, 156, 'VERY_GOOD_CONDITION', 'Veste', 'ALLSEASON', false, 1, 1);
