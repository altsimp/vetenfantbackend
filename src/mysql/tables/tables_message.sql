-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS message;
CREATE TABLE message(
    id INT PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR (500) NOT NULL,
    date DATE NOT NULL,
    id_sender INT,
    id_product INT,
    Foreign Key (id_sender) REFERENCES user(id) ON DELETE SET NULL,
    Foreign Key (id_product) REFERENCES product(id) ON DELETE SET NULL
);