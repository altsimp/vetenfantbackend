-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS transaction;

CREATE TABLE transaction(
    id INT PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR (500) NOT NULL,
    date DATE NOT NULL,
    id_sender INT,
    id_product INT,
    Foreign Key (id_sender) REFERENCES user(id) ON DELETE SET NULL,
    Foreign Key (id_product) REFERENCES product(id) ON DELETE SET NULL,
    price INT NOT NULL, 
    status ENUM ('PENDING','PROCESSING','SHIPPED','DELIVERED','CANCELLED','RETURNED') NOT NULL,
    accepted_date DATE,
    shipping_date DATE,
    delivered_date DATE,
    discount DOUBLE
);
