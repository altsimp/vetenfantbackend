-- Active: 1698333674662@@127.0.0.1@3306@simplon_vetenfant
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(55) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(1000) NOT NULL,
    role VARCHAR (55),
    phoneNumber VARCHAR(25),
    name VARCHAR (55),
    firstname VARCHAR (55),
    description VARCHAR (1000),
    profileImage VARCHAR (2000),
    evaluation FLOAT
)