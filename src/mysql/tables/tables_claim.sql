-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS claim;

CREATE TABLE claim(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    description VARCHAR(1000) NOT NULL,
    date DATE NOT NULL,
    status BOOLEAN NOT NULL,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE SET NULL
);