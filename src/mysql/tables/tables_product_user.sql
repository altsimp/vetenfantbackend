-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS product_user;

CREATE TABLE product_user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_user INT,
    id_product INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    Foreign Key (id_product) REFERENCES product(id) ON DELETE CASCADE
);