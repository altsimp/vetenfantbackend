-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS adress;

CREATE TABLE adress(
    id INT PRIMARY KEY AUTO_INCREMENT,
    postalCode VARCHAR(55) NOT NULL,
    city VARCHAR(255) NOT NULL,
    street VARCHAR(255) NOT NULL,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE
);