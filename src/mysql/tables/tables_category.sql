-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS category;

CREATE TABLE category(
    id INT PRIMARY KEY AUTO_INCREMENT,
    type VARCHAR(55) NOT NULL
);