-- Active: 1711560814202@@localhost@3306@alt6_vetenfant
DROP TABLE IF EXISTS image;

CREATE TABLE image(
    id INT PRIMARY KEY AUTO_INCREMENT,
    src VARCHAR(1000) NOT NULL,
    title VARCHAR (255) NOT NULL,
    alt VARCHAR (55) NOT NULL,
    id_product INT,
    FOREIGN KEY (id_product) REFERENCES product(id) ON DELETE CASCADE
);