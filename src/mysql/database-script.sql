-- Active: 1712398942119@@127.0.0.1@3306@alt6_vetenfant
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS adress;
DROP TABLE IF EXISTS paymentInfo;
DROP TABLE IF EXISTS claim;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS product_user;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS transaction;

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    accountName VARCHAR(55) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(1000) NOT NULL,
    phoneNumber VARCHAR(25),
    name VARCHAR (55),
    firstname VARCHAR (55),
    description VARCHAR (1000),
    profileImage VARCHAR (2000),
    evaluation FLOAT,
    role VARCHAR (55)
);
CREATE TABLE adress(
    id INT PRIMARY KEY AUTO_INCREMENT,
    postalCode VARCHAR(55) NOT NULL,
    city VARCHAR(255) NOT NULL,
    street VARCHAR(255) NOT NULL,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE
);
CREATE TABLE claim(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    description VARCHAR(1000) NOT NULL,
    date DATE NOT NULL,
    status BOOLEAN NOT NULL,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE SET NULL
);
CREATE TABLE category(
    id INT PRIMARY KEY AUTO_INCREMENT,
    type VARCHAR(55) NOT NULL
);
CREATE TABLE product(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(55) NOT NULL,
    gender ENUM('BOY', 'GIRL', 'UNISEX') NOT NULL,
    price INT NOT NULL,
    size INT NOT NULL,
    description VARCHAR (1000) NOT NULL,
	isSold BOOLEAN NOT NULL,
    season ENUM('SPRING','SUMMER','AUTUMN','WINTER','ALLSEASON','SPRING_SUMMER','SUMMER_AUTUMN','AUTUMN_WINTER','SPRING_AUTUMN'),
    state ENUM('NEW_WITH_TAGS','NEW_WITHOUT_TAGS','VERY_GOOD_CONDITION', 'GOOD_CONDITION', 'STATISFACTORY'),
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    id_category INT,
    Foreign Key (id_category) REFERENCES category(id) ON DELETE CASCADE
);

CREATE TABLE product_user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    id_product INT,
    Foreign Key (id_product) REFERENCES product(id) ON DELETE CASCADE
);

CREATE TABLE image(
    id INT PRIMARY KEY AUTO_INCREMENT,
    src VARCHAR(1000) NOT NULL,
    title VARCHAR (255) NOT NULL,
    alt VARCHAR (55) NOT NULL,
    id_product INT,
    FOREIGN KEY (id_product) REFERENCES product(id) ON DELETE CASCADE
);
CREATE TABLE message(
    id INT PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR (500) NOT NULL,
    date DATE NOT NULL,
    id_sender INT,
    id_product INT,
    id_receiver INT,
    Foreign Key (id_sender) REFERENCES user(id) ON DELETE CASCADE,
    Foreign Key (id_receiver) REFERENCES user(id) ON DELETE CASCADE,
    Foreign Key (id_product) REFERENCES product(id) ON DELETE CASCADE
);
CREATE TABLE transaction(
    id INT PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR (500) NOT NULL,
    date DATE NOT NULL,
    id_sender INT,
    id_product INT,
    Foreign Key (id_sender) REFERENCES user(id) ON DELETE SET NULL,
    Foreign Key (id_product) REFERENCES product(id) ON DELETE SET NULL,
    price INT NOT NULL, 
    status VARCHAR(50),
    accepted_date DATE,
    shipping_date DATE,
    delivered_date DATE,
    discount double 
);

INSERT INTO user (accountName, email, password, phoneNumber, name, firstname, description, profileImage, evaluation,role) VALUES
('Chems', 'chems.mail@hotmail.com', '1234', '0666778899', 'Meziou', 'Chems', 'Ma premiere description', 'https://media.licdn.com/media/AAYQAQSOAAgAAQAAAAAAAB-zrMZEDXI2T62PSuT6kpB6qg.png', 4),
('Hisami', 'hisame.mail@hotmail.com', '4567', '0677889900', 'Stolz', 'Hisami', 'Ma deuxiémeh description', 'https://media.licdn.com/dms/image/D4E35AQGQe9it0_DlZA/profile-framedphoto-shrink_800_800/0/1673642659527?e=1698861600&v=beta&t=XKOjw44TY8hacldYvkddcqL9TOC44L4u3VRs0_pbVnw', 4.5),
('Alicia', 'Alicia.mail@hotmail.com', '8901', '0688990011', 'Charef', 'Alicia', 'Ma troisiéme description', 'https://media.licdn.com/dms/image/D4E03AQE410JnpzNK2Q/profile-displayphoto-shrink_800_800/0/1676626614329?e=1703721600&v=beta&t=FS8-fa0sV_Lb2dTBdUHsnc0wR6VA4i0vaGfraUxPinQ', 4);

INSERT INTO adress(postalCode, city, street, id_user) VALUES
('69100', 'Villeurbanne', 'Rue Montaland', 1),
('69006', 'Lyon', 'Rue Gambetta', 2),
('69530', 'Brignais', 'Rue Primat', 3);

INSERT INTO claim(title, description, date, status, id_user) VALUES
('Chems claim', 'Ma réclamation', '2023-09_23', true, 1),
('Hisami claim', 'Sa réclamation', '2023-10-11', False, 2),
('Alicia claim', 'Sa réclamation', '2023-09_21', true, 3);


INSERT INTO category(type) VALUES
('pants'),
('skirt'),
('pyjama');

INSERT INTO product(name, gender, price, size, description, isSold, season, state, id_user, id_category) VALUES
('Veste', 'BOY', 10, 1, 'Veste', false, 'SPRING','NEW_WITH_TAGS',1, 1),
('Pantalon', 'BOY', 15, 3, 'Pantalon', false, 'SUMMER', 'NEW_WITH_TAGS', 2, 2),
('T-shirt', 'GIRL', 5, 6, 'T-shirt', false,'AUTUMN', 'NEW_WITH_TAGS', 3, 3),
('Veste', 'UNISEX', 1, 12, 'Veste', false,'WINTER', 'VERY_GOOD_CONDITION', 1, 1),
('Veste', 'BOY', 10, 1, 'Veste', false,'SPRING_SUMMER', 'NEW_WITH_TAGS' ,1, 1);

INSERT INTO product_user(id_user, id_product) VALUES
('1','1'), ('2','2'), ('3','3');

INSERT INTO image(src, title, alt,id_product) VALUES
('https://media.licdn.com/media/AAYQAQSOAAgAAQAAAAAAAB-zrMZEDXI2T62PSuT6kpB6qg.png', 'Image de Chems', 'image',1),
('https://media.licdn.com/dms/image/D4E35AQGQe9it0_DlZA/profile-framedphoto-shrink_800_800/0/1673642659527?e=1698861600&v=beta&t=XKOjw44TY8hacldYvkddcqL9TOC44L4u3VRs0_pbVnw', 'Image de Hisami', 'image',2),
('https://media.licdn.com/dms/image/D4E03AQE410JnpzNK2Q/profile-displayphoto-shrink_800_800/0/1676626614329?e=1703721600&v=beta&t=FS8-fa0sV_Lb2dTBdUHsnc0wR6VA4i0vaGfraUxPinQ', 'Image de Alicia', 'image',3);


INSERT INTO message(content, date, id_sender, id_product) VALUES
('Hello moi c\' Chems', '2023-10-25', 1, 2), ('Hello moi c\' Hisami', '2023-10-25', 2, 3), ('Hello moi c\' Alicia', '2023-10-25', 3, 1);

INSERT INTO transaction(content, date, id_sender, id_product,price,status) VALUES 
('Hello moi c\' Chems', '2023-10-25', 1, 2,15,"PENDING"),
('Hello moi c\' Hisami', '2023-10-25', 2, 3,10,"PENDING"), 
('Hello moi c\' Hisami', '2023-10-25', 2, 3,20,"PENDING");
