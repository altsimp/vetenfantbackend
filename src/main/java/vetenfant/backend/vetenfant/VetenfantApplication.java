package vetenfant.backend.vetenfant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VetenfantApplication {

	public static void main(String[] args) {
		SpringApplication.run(VetenfantApplication.class, args);
	}
}
