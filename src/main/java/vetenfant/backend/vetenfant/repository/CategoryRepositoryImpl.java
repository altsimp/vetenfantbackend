package vetenfant.backend.vetenfant.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;
import vetenfant.backend.vetenfant.entity.Category;
import vetenfant.backend.vetenfant.repository.interfaces.CategoryRepository;

@Repository
public class CategoryRepositoryImpl extends AbstractDao<Category> implements CategoryRepository{
    
     public CategoryRepositoryImpl(){
        super(
            "INSERT INTO category (type) VALUES (?)",    
            "SELECT * FROM category",
            "SELECT * FROM category WHERE id=?",
            "UPDATE category SET type=? WHERE id=?",
            "DELETE FROM category WHERE id=?"
        );
    }

    @Override
    protected Category sqlToEntity(ResultSet rs) throws SQLException {
        try {
            return new Category(
                rs.getInt("id"),
                rs.getString("type"));
        } catch (SQLException e) {
            e.printStackTrace(); 
            throw new SQLException("Error while mapping ResultSet to User entity: " + e.getMessage());
        }
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Category entity) throws SQLException {
        try{
            stmt.setString(1,entity.getType());
        }catch(SQLException e){
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValues'");
        }     
    }

    @Override
    protected void setEntityId(Category entity, int id){
            entity.setId(id);    
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Category entity) throws SQLException {
        try{
            entityBindValues(stmt, entity);
            stmt.setInt(2, entity.getId());     
        }catch(SQLException e){
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValuesWithId'");
        }
    }
}
