package vetenfant.backend.vetenfant.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Adress;
import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.repository.interfaces.AdressRepository;

@Repository
public class AdressRepositoryImpl extends AbstractDao<Adress> implements AdressRepository {

    public AdressRepositoryImpl() {
        super(
                "INSERT INTO adress (postalCode, city, street, id_user) VALUES (?,?,?,?)",
                "SELECT * FROM adress",
                "SELECT * FROM adress WHERE id=?",
                "UPDATE adress SET postalCode=?,city=?,street=?,id_user=? WHERE id=?",
                "DELETE FROM adress WHERE id=?");
    }

    @Override
    protected Adress sqlToEntity(ResultSet rs) throws SQLException {
        try {
            return new Adress(
                    rs.getInt("id"),
                    rs.getString("postalCode"),
                    rs.getString("city"),
                    rs.getString("street"),
                    rs.getInt("id_user"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error while mapping ResultSet to Adress entity: " + e.getMessage());
        }
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Adress entity) throws SQLException {
        try {
            stmt.setString(1, entity.getPostalCode());
            stmt.setString(2, entity.getCity());
            stmt.setString(3, entity.getStreet());
            stmt.setInt(4, entity.getUserId());
        } catch (SQLException e) {
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValues'");
        }
    }

    @Override
    protected void setEntityId(Adress entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Adress entity) throws SQLException {
        try {
            entityBindValues(stmt, entity);
            stmt.setInt(5, entity.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValuesWithId'");
        }
    }
}
