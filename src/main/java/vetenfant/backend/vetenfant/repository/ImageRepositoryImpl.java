package vetenfant.backend.vetenfant.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Category;
import vetenfant.backend.vetenfant.entity.Image;
import vetenfant.backend.vetenfant.entity.Product;
import vetenfant.backend.vetenfant.enums.GenderEnum;
import vetenfant.backend.vetenfant.enums.SeasonEnum;
import vetenfant.backend.vetenfant.enums.StateEnum;
import vetenfant.backend.vetenfant.repository.interfaces.ImageRepository;

@Repository
public class ImageRepositoryImpl extends AbstractDao<Image> implements ImageRepository{

    public ImageRepositoryImpl(){
        super(
            "INSERT INTO image (src, title, alt, id_product) VALUES (?,?,?,?)",    
            "SELECT * FROM image INNER JOIN product ON image.id_product = product.id",
            "SELECT * FROM image INNER JOIN product ON image.id_product = product.id WHERE image.id=?",
            "UPDATE image SET src=?,title=?,alt=?,id_product WHERE id=?",
            "DELETE FROM image WHERE id=?"
        );
    }

    @Override
    protected Image sqlToEntity(ResultSet rs) throws SQLException {
        try {
            return new Image(
                rs.getInt("id"),
                rs.getString("src"),
                rs.getString("title"),
                rs.getString("alt"),
                new Product(
                    rs.getInt("product.id"),
                    rs.getString("product.name"),
                    GenderEnum.fromString(rs.getString("product.gender")),
                    rs.getDouble("price"),
                    rs.getInt("size"),
                    rs.getString("product.description"),
                    rs.getBoolean("isSold"),
                    null, 
                    SeasonEnum.fromString(rs.getString("season")),
                    StateEnum.fromString(rs.getString("state"))   
                )
            );
        } catch (SQLException e) {
            e.printStackTrace(); 
            throw new SQLException("Error while mapping ResultSet to Image entity: " + e.getMessage());
        }
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Image entity) throws SQLException {
        try{
            stmt.setString(1,entity.getSrc());
            stmt.setString(2,entity.getTitle());
            stmt.setString(3,entity.getAlt());
            stmt.setInt(4, entity.getProduct().getId());
        }catch(SQLException e){
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValues'");
        }     
    }

    @Override
    protected void setEntityId(Image entity, int id){
            entity.setId(id);    
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Image entity) throws SQLException {
        try{
            entityBindValues(stmt, entity);
            stmt.setInt(5, entity.getId());     
        }catch(SQLException e){
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValuesWithId'");
        }
    }
}
