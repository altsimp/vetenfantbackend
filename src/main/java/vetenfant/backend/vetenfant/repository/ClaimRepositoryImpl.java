package vetenfant.backend.vetenfant.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Claim;
import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.repository.interfaces.ClaimRepository;

@Repository
public class ClaimRepositoryImpl extends AbstractDao<Claim> implements ClaimRepository{
    
    public ClaimRepositoryImpl() {
        super(
            "INSERT INTO claim (title, description, date, status, id_user)VALUES (?,?,?,?,?)",    
            "SELECT * FROM claim INNER JOIN user ON claim.id_user = user.id",
            "SELECT * FROM claim INNER JOIN user ON claim.id_user= user.id WHERE claim.id=?",
            "UPDATE claim SET title=?,description=?,date=?, status=?,id_user=? WHERE id=?",
            "DELETE FROM claim WHERE id=?"

        );
    }

    @Override
    protected Claim sqlToEntity(ResultSet rs) throws SQLException {
     
        try {
            return new Claim(
                rs.getInt("claim.id"),
                rs.getString("title"),
                rs.getString("description"),
                rs.getDate("date").toLocalDate(),
                rs.getBoolean("status"),
                new User(
                    rs.getInt("user.id"),
                    rs.getString("accountName"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("phoneNumber"),
                    rs.getString("name"),
                    rs.getString("firstname"),
                    rs.getString("description"),
                    rs.getDouble("evaluation"),
                    rs.getString("role")
                )                              
        );
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error while mapping ResultSet to Claim entity: " + e.getMessage());
        }
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Claim entity) throws SQLException {
       try {
            stmt.setString(1, entity.getTitle());
            stmt.setString(2, entity.getDescription());
            stmt.setDate(3, java.sql.Date.valueOf(entity.getDate()));
            stmt.setBoolean(4, entity.getStatus());
            stmt.setInt(5, entity.getUser().getId());       
       } catch (SQLException e) {
        throw new UnsupportedOperationException("Unimplemented method 'entityBindValues'");
       }
    }

    @Override
    protected void setEntityId(Claim entity, int id) {
        entity.setId(id);  
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Claim entity) throws SQLException {
        try{
            entityBindValues(stmt, entity);
            stmt.setInt(6, entity.getId());     
        }catch(SQLException e){
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValuesWithId'");
        }

    }
}
