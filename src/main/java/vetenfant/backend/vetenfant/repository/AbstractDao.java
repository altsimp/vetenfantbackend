package vetenfant.backend.vetenfant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import vetenfant.backend.vetenfant.repository.interfaces.CrudRepository;

public abstract class AbstractDao<E> implements CrudRepository<E> {

    @Autowired
    private DataSource dataSource;

    private String addSQL;
    private String getAllSQL;
    private String getByIdSQL;
    private String updateSQL;
    private String deleteSQL;

    public AbstractDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        this.addSQL = addSQL;
        this.getAllSQL = getAllSQL;
        this.getByIdSQL = getByIdSQL;
        this.updateSQL = updateSQL;
        this.deleteSQL = deleteSQL;
    }

    @Override
    public boolean persist(E entity) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(addSQL, Statement.RETURN_GENERATED_KEYS);
            entityBindValues(stmt, entity);
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            setEntityId(entity, rs.getInt(1));

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<E> findAll() {
        List<E> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getAllSQL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                E entity = sqlToEntity(rs);
                list.add(entity);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public E find(Integer id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getByIdSQL);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                E entity = sqlToEntity(rs);
                return entity;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    protected Optional<E> findOneBy(String query, Object param) throws SQLException{
        try(Connection connection = dataSource.getConnection()){
        PreparedStatement stmt = connection.prepareStatement(query);
        stmt.setObject(1, param);
        ResultSet rs= stmt.executeQuery();
        if (rs.next()) {
            E entity = sqlToEntity(rs);
            return Optional.ofNullable(entity);
        } else {
            return Optional.empty();
        }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("Error executing database query", e);
        }
    }


    @Override
    public boolean update(E entity) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(updateSQL);
            entityBindValuesWithId(stmt, entity);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(deleteSQL);
            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Méthode qui prend une ligne de résultat d'un ResultSet pour le convertir en
     * une instance de l'entité ciblée
     */
    protected abstract E sqlToEntity(ResultSet rs) throws SQLException;

    /**
     * Méthode abstraite qui contiendra les différents set... pour assigner des
     * valeurs
     * aux placeholders de la requête add
     */
    protected abstract void entityBindValues(PreparedStatement stmt, E entity) throws SQLException;

    /**
     * Méthode abstraite qui indiquera comment on assigne un id à l'entité ciblée
     */
    protected abstract void setEntityId(E entity, int id);

    /**
     * Même chose que entityBindValues mais avec un set de l'id en plus (pour un
     * update)
     */
    protected abstract void entityBindValuesWithId(PreparedStatement stmt, E entity) throws SQLException;

}
