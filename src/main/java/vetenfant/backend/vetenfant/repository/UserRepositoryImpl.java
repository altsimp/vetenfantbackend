package vetenfant.backend.vetenfant.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.repository.interfaces.UserRepository;

@Repository
public class UserRepositoryImpl extends AbstractDao<User> implements UserRepository{

    public UserRepositoryImpl(){
        super(
            "INSERT INTO user (accountName,email,password,phoneNumber,name,firstname,description,profileImage,evaluation,role) VALUES (?,?,?,?,?,?,?,?,?,?)",
                "SELECT * FROM user",
                "SELECT * FROM user WHERE id=?",
                "UPDATE user SET UserName=?,email=?,password=?,phoneNumber=?,name=?,firstname=?,description=?,profileImage=?,evaluation=?,role=? WHERE id=?",
                "DELETE FROM user WHERE id=?"       
        );
    }

    @Override
    protected User sqlToEntity(ResultSet rs) throws SQLException {
        try {
            return new User(
                    rs.getInt("id"),
                    rs.getString("accountName"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("phoneNumber"),
                    rs.getString("name"),
                    rs.getString("firstname"),
                    rs.getString("description"),
                    rs.getDouble("evaluation"),
                    rs.getString("role")
                );
                    
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error about information of entity User" + e.getMessage());
        }

    }
    @Override
    protected void entityBindValues(PreparedStatement stmt, User entity) throws SQLException {
        try {
            stmt.setString(1, entity.getAccountName());
            stmt.setString(2, entity.getEmail());
            stmt.setString(3, entity.getPassword());
            stmt.setString(4, entity.getPhoneNumber());
            stmt.setString(5, entity.getName());
            stmt.setString(6, entity.getFirstName());
            stmt.setString(7, entity.getDescription());
            stmt.setString(8, entity.getProfileImage());
            stmt.setDouble(9, entity.getEvaluation());
            stmt.setString(10, entity.getRole());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setEntityId(User entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, User entity) throws SQLException {
        try{
            entityBindValues(stmt, entity);
            stmt.setInt(11, entity.getId());
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<User> findByEmail(String email) throws SQLException {
        return findOneBy("SELECT * FROM user WHERE email=?", email);
    } 
}
