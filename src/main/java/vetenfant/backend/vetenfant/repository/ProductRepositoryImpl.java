package vetenfant.backend.vetenfant.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Category;
import vetenfant.backend.vetenfant.entity.Product;
import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.enums.GenderEnum;
import vetenfant.backend.vetenfant.enums.SeasonEnum;
import vetenfant.backend.vetenfant.enums.SizeEnum;
import vetenfant.backend.vetenfant.enums.StateEnum;
import vetenfant.backend.vetenfant.repository.interfaces.ProductRepository;

@Repository
public class ProductRepositoryImpl extends AbstractDao<Product> implements ProductRepository {

    public ProductRepositoryImpl() {
        super(
                "INSERT INTO product (name,gender,price,size,description,isSold,id_category,id_user,season,state) VALUES (?,?,?,?,?,?,?,?,?,?)",
                "SELECT * FROM product INNER JOIN category ON product.id_category=category.id INNER JOIN user ON product.id_user=user.id",
                "SELECT * FROM product INNER JOIN category ON product.id_category=category.id INNER JOIN user ON product.id_user=user.id WHERE product.id=?",
                "UPDATE product SET name=?,gender=?,price=?, size=?,description=?,isSold=?,id_category=?,id_user=?,season=?,state=? WHERE id=?",
                "DELETE FROM product WHERE id=?"
            );
    }


    protected Product sqlToEntity(ResultSet rs) throws SQLException {
        try {
            Product product = new Product(
                rs.getInt("product.id"),
                rs.getString("name"),
                GenderEnum.fromString(rs.getString("product.gender")),
                rs.getDouble("price"),
                rs.getInt("size"),
                rs.getString("description"),
                rs.getBoolean("isSold"),
                new Category(rs.getInt("id_category"), 
                rs.getString("category.type")),
                new User(
                    rs.getInt("user.id"),
                    rs.getString("accountName"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("phoneNumber"),
                    rs.getString("name"),
                    rs.getString("firstname"),
                    rs.getString("description"),
                    rs.getDouble("evaluation"),
                    rs.getString("role")
                ),
                SeasonEnum.fromString(rs.getString("season")),
                StateEnum.fromString(rs.getString("state"))
            );
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error while mapping ResultSet to Product entity: " + e.getMessage());
        }
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Product entity) throws SQLException {
        try {
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getGender().name());
            stmt.setDouble(3, entity.getPrice());
            stmt.setInt(4, entity.getSize().intValue());
            stmt.setString(5, entity.getDescription());
            stmt.setBoolean(6, entity.getSold());
            stmt.setInt(7, entity.getCategory().getId());
            stmt.setInt(8, entity.getSeller().getId());
            stmt.setString(9, entity.getSeason().name());
            stmt.setString(10, entity.getState().name());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValues'");
        }

    }

    @Override
    protected void setEntityId(Product entity, int id) {
        entity.setId(id);
    }


    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Product entity) throws SQLException {
        try {
            entityBindValues(stmt, entity);
            stmt.setInt(11, entity.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UnsupportedOperationException("Unimplemented method 'entityBindValuesWithId'");
        }
    }


    


}
