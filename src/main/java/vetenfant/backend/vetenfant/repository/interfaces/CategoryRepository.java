package vetenfant.backend.vetenfant.repository.interfaces;

import org.springframework.stereotype.Repository;
import vetenfant.backend.vetenfant.entity.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category>{ 
}

