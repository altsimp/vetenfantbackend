package vetenfant.backend.vetenfant.repository.interfaces;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Image;

@Repository
public interface ImageRepository extends CrudRepository<Image>{
    
}
