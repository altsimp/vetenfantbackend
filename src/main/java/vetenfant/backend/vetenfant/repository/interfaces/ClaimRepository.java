package vetenfant.backend.vetenfant.repository.interfaces;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Claim;

@Repository
public interface ClaimRepository extends CrudRepository<Claim>{
    
}
