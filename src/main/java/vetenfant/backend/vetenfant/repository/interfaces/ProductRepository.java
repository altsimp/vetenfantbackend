package vetenfant.backend.vetenfant.repository.interfaces;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product>{ 

}

