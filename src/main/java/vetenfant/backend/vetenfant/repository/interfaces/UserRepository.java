package vetenfant.backend.vetenfant.repository.interfaces;

import java.sql.SQLException;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import vetenfant.backend.vetenfant.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User>{
    Optional<User> findByEmail(String email) throws SQLException;
}