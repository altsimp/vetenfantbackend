package vetenfant.backend.vetenfant.repository.interfaces;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
import vetenfant.backend.vetenfant.entity.Adress;

@Repository
public interface AdressRepository extends CrudRepository<Adress>{ 
  
}

