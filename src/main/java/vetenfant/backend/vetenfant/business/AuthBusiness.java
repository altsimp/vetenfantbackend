package vetenfant.backend.vetenfant.business;

import java.sql.SQLException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.repository.interfaces.UserRepository;

@Service
public class AuthBusiness implements UserDetailsService{
    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRepository userRepo;

    public void register(User user) throws Exception {
        //A la place de ce bout de code ci dessous, on pourrait utiliser notre loadUserByUsername
        Optional<User> existingUser = userRepo.findByEmail(user.getEmail());
        if(existingUser.isPresent()) {
            throw new Exception("User already exist");
        }

        String hashedPassword = hasher.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setRole("ROLE_USER");
        userRepo.persist(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Optional<User> userOptional = userRepo.findByEmail(username);
            if (userOptional.isPresent()) {
                return userOptional.get();
            } else {
                throw new UsernameNotFoundException("User not found with email: " + username);
            }
        } catch (SQLException e) {
            e.printStackTrace(); 
            throw new UsernameNotFoundException("Error retrieving user with email: " + username, e);
        }
    }
}

