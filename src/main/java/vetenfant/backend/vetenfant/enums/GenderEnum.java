package vetenfant.backend.vetenfant.enums;

public enum GenderEnum {
    Girl,
    Boy,
    Unisex;
    
    public static GenderEnum fromString(String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        for (GenderEnum gender : values()) {
            if (gender.name().equalsIgnoreCase(value)) {
                return gender;
            }
        }
        throw new IllegalArgumentException("Invalid gender value: " + value);
    }
}
