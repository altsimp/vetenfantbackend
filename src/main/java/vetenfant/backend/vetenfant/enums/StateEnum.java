package vetenfant.backend.vetenfant.enums;

public enum StateEnum {
    NEW_WITH_TAGS,
    NEW_WITHOUT_TAGS,
    VERY_GOOD_CONDITION,
    GOOD_CONDITION,
    STATISFACTORY;  
    
    public static StateEnum fromString(String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        for (StateEnum state : values()) {
            if (state.name().equalsIgnoreCase(value)) {
                return state;
            }
        }
        throw new IllegalArgumentException("Invalid state value: " + value);
    }
}
