package vetenfant.backend.vetenfant.enums;

import java.time.LocalDate;

public enum SeasonEnum {
    SPRING,
    SUMMER,
    AUTUMN,
    WINTER,
    ALLSEASON,
    SPRING_SUMMER,
    SUMMER_AUTUMN,
    AUTUMN_WINTER,
    WINTER_SPRING,
    SPRING_AUTUMN;

    public static SeasonEnum fromString(String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        for (SeasonEnum season : values()) {
            if (season.name().equalsIgnoreCase(value)) {
                return season;
            }
        }
        throw new IllegalArgumentException("Invalid season value: " + value);
    }

    public static SeasonEnum getSeason(LocalDate date) {
        int month = date.getMonthValue();
        switch (month) {
            case 3:
            case 4:
            case 5:
                return SPRING;
            case 6:
            case 7:
            case 8:
                return SUMMER;
            case 9:
            case 10:
            case 11:
                return AUTUMN;
            default:
                return WINTER;
        }
    }
}

