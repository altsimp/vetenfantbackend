 package vetenfant.backend.vetenfant.enums;

public enum SizeEnum {
    NEWBORN(0),
    ONEMONTH(1),
    THREEMONTHS(3),
    SIXMONTHS(6),
    NINEMONTHS(9),
    TWELVEMONTHS(12),
    EIGHTEENMONTHS(18), 
    TWOYEARS(24),
    THREEYEARS(36),
    FORYEARS(48),
    FIVEYEARS(60),
    SIXYEARS(72),
    SEVENYEARS(84),
    EIGHTYEARS(96),
    NINEYEARS(108),
    TENYEARS(120),
    ELEVEYEARS(132),
    TWELVEYEARS(144),
    THIRTEENYEARS(156),
    FOURTEENYEARS(168),
    SIXTEENYEARS(192);
 
    private final int value;

    SizeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static SizeEnum fromValue(int value) {
    
        for (SizeEnum size : values()) {
            if (size.value == value) {
                return size;
            }
        }
        throw new IllegalArgumentException("Invalid size value: " + value);
    }

    public static SizeEnum getSizeForAge(int ageInMonths) {
        for (SizeEnum size : values()) {
            if (ageInMonths <= size.getValue()) {
                return size;
            }
        }
        return getClosestLargerSize(ageInMonths);
    }

    private static SizeEnum getClosestLargerSize(int ageInMonths) {
        SizeEnum closestLargerSize = null;
        for (SizeEnum size : values()) {
            if (size.getValue() > ageInMonths) {
                if (closestLargerSize == null || size.getValue() < closestLargerSize.getValue()) {
                    closestLargerSize = size;
                }
            }
        }
        return closestLargerSize;
    }

    public static Object fromValue(SizeEnum size) {
        return null;
    }



}
