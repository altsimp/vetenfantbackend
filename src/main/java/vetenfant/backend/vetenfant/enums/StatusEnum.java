package vetenfant.backend.vetenfant.enums;

public enum StatusEnum {
    /* for transaction */
    PENDING,
    PROCESSING,
    SHIPPED,
    DELIVERED,
    CANCELLED,
    RETURNED;

    public static StatusEnum fromString(String value) {
        if (value == null || value.trim().isEmpty()) {
        return null;
    }

        for (StatusEnum status : values()) {
            if (status.name().equalsIgnoreCase(value)) {
                return status;
            }
        }
        throw new IllegalArgumentException("Invalid status value: " + value);
    }
}
