package vetenfant.backend.vetenfant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import vetenfant.backend.vetenfant.entity.Claim;
import vetenfant.backend.vetenfant.repository.interfaces.ClaimRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;


@CrossOrigin
@RestController
@RequestMapping("/api/claim")
public class ClaimController {
    
    @Autowired
    private ClaimRepository repo;

    @GetMapping
    public List<Claim> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Claim one(@PathVariable int id) {
        Claim claim = repo.find(id);
        if(claim == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return claim;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Claim add(@Valid @RequestBody Claim claim) {
        repo.persist(claim);
        return claim;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public Claim update(@PathVariable int id, @RequestBody Claim claim) {
        Claim toUpdate = one(id);
        if(claim.getTitle() != null) {
            toUpdate.setTitle(claim.getTitle());
        } 
        if(claim.getDescription() != null) {
            toUpdate.setDescription(claim.getDescription());
        }
        if(claim.getDate() != null) {
            toUpdate.setDate(claim.getDate());
        } 
        if(claim.getStatus()) {
            toUpdate.setStatus(claim.getStatus());
        }
        if(claim.getUser() != null) {
            toUpdate.setUser(claim.getUser());
        }
        repo.update(toUpdate);
        return toUpdate;
    }
}
