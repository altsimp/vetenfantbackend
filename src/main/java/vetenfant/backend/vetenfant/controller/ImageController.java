package vetenfant.backend.vetenfant.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import vetenfant.backend.vetenfant.entity.Image;
import vetenfant.backend.vetenfant.repository.interfaces.ImageRepository;

@CrossOrigin
@RestController
@RequestMapping("/api/image")
public class ImageController {

    @Autowired
    private ImageRepository repo;

    @GetMapping
    public List<Image> all(){
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Image one (@PathVariable int id){
        Image image = repo.find(id);
        if(image == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return image;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Image add(@Valid @RequestBody Image image) {
        repo.persist(image);
        return image;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping
    public Image update(@RequestBody Image image) throws IOException {
        Image toUpdate = one(image.getId());
        if(image.getSrc() != null) {
            toUpdate.setSrc(image.getSrc());
        } 
        if(image.getTitle() != null) {
            toUpdate.setTitle(image.getTitle());
        }
        if(image.getAlt() != null) {
            toUpdate.setAlt(image.getAlt());
        } 
        repo.update(toUpdate);
        return toUpdate;
    }
}