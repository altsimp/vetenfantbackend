package vetenfant.backend.vetenfant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import vetenfant.backend.vetenfant.entity.Category;
import vetenfant.backend.vetenfant.repository.interfaces.CategoryRepository;

@CrossOrigin
@RestController
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    private CategoryRepository repo;

    @GetMapping
    public List<Category> all(){
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Category one(@PathVariable int id) {
        Category category = repo.find(id);
        if(category == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return category;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Category add(@Valid @RequestBody Category category) {
        repo.persist(category);
        return category;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public Category update(@PathVariable int id, @RequestBody Category category) {
        Category toUpdate = one(id);
        if(category.getType() != null) {
            toUpdate.setType(category.getType());
        } 
        repo.update(toUpdate);
        return toUpdate;
    }
}

