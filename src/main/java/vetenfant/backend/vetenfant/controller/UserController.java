package vetenfant.backend.vetenfant.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.repository.interfaces.UserRepository;

@CrossOrigin
@RestController //annotation obligatoire  pour faire un contrôleur de type rest
@RequestMapping("/api/user") /* //permet de mettre un prefix à toutes les routes définies dans ce contrôleur*/
public class UserController {
    @Autowired // Permet d'injecter un autre composant connu de spring dans cette classe (spring se charge de l'instanciation)
    private UserRepository repo;

    @GetMapping
    public List<User> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public User one(@PathVariable int id) {
        User user = repo.find(id);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return user;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User add(@Valid @RequestBody User user) {
        repo.persist(user);
        return user;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public User update(@PathVariable int id, @RequestBody User user){
        User toUpdate = one(id);
        if(user.getAccountName() != null) {
            toUpdate.setAccountName(user.getAccountName());
        }
        if(user.getPhoneNumber() != null) {
            toUpdate.setPhoneNumber(user.getPhoneNumber());
        }
        if(user.getName() != null) {
            toUpdate.setName(user.getName());
        }
        if(user.getFirstName() != null) {
            toUpdate.setFirstName(user.getFirstName());
        }
        if(user.getDescription() != null) {
            toUpdate.setDescription(user.getDescription());
        }
        if(user.getEvaluation() != null) {
            toUpdate.setEvaluation(user.getEvaluation());
        }
        repo.update(toUpdate);
        return toUpdate;
    }
}
