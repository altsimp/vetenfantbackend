package vetenfant.backend.vetenfant.controller;

import vetenfant.backend.vetenfant.entity.Adress;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import vetenfant.backend.vetenfant.repository.interfaces.AdressRepository;

@CrossOrigin
@RestController
@RequestMapping("/api/adress")
public class AdressController {

    @Autowired
    private AdressRepository repo;

    @GetMapping
    public List<Adress> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Adress one(@PathVariable int id) {
        Adress adress = repo.find(id);
        if (adress == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return adress;
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Adress add(@Valid @RequestBody Adress adress) {
        repo.persist(adress);
        return adress;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public Adress update(@PathVariable int id, @RequestBody Adress adress) {
        Adress toUpdate = one(id);
        if(adress.getPostalCode() != null) {
            toUpdate.setPostalCode(adress.getPostalCode());
        }
        if(adress.getCity() != null) {
            toUpdate.setCity(adress.getCity());
        }
        if(adress.getStreet() != null) {
            toUpdate.setStreet(adress.getStreet());
        }   
        repo.update(toUpdate);
        return toUpdate;
    }    
}
