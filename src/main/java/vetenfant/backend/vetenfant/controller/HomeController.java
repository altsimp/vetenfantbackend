package vetenfant.backend.vetenfant.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController

/* @RequestMapping("/") */
public class HomeController {


    @GetMapping("/")
    public String home() {
       /*  System.out.println("un message en console dans le contrôleur"); */
        return "Bienvenue sur le site VetEnfant";
    }

}
