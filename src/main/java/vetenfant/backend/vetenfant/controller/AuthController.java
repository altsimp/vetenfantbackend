package vetenfant.backend.vetenfant.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import vetenfant.backend.vetenfant.business.AuthBusiness;
import vetenfant.backend.vetenfant.entity.User;
import vetenfant.backend.vetenfant.repository.interfaces.UserRepository;
import vetenfant.backend.vetenfant.security.AuthResponse;
import vetenfant.backend.vetenfant.security.Credentials;
import vetenfant.backend.vetenfant.security.JwtUtils;

@RestController
public class AuthController {
    @Autowired
    private AuthBusiness authBusiness;
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/api/register")
    public User postUser(@RequestBody User user){
        try {
            authBusiness.register(user);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return user;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/api/login")
    public AuthResponse postLogin(@RequestBody Credentials credentials) {
        
        UserDetails user = authBusiness.loadUserByUsername(credentials.email);
        if(!passwordEncoder.matches(credentials.password, user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login/Password doesn't match or exist");
        }
        return new AuthResponse(user, jwtUtils.generateJwt(user));
    }

    @GetMapping("/api/account")
    public UserDetails getAccount(@AuthenticationPrincipal UserDetails user) {
        return user;
    }


}

