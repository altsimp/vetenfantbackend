package vetenfant.backend.vetenfant.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;


public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String type;
    @OneToMany(mappedBy = "category")
    private List<Product> products = new ArrayList<>();
    
    public Category(Integer id) {
      this.id = id;
    }
    public Category(String type) {
        this.type = type;
    }
    public Category(Integer id, String type) {
        this.id = id;
        this.type = type;
    }
    public Category() {
    }
    public Integer getId() {
      return id;
    }
    public void setId(Integer id) {
      this.id = id;
    }
    public String getType() {
      return type;
    }
    public void setType(String type) {
      this.type = type;
    }
    public List<Product> getProducts() {
      return products;
    }
    public void setProducts(List<Product> products) {
      this.products = products;
    }
}

