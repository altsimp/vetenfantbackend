package vetenfant.backend.vetenfant.entity;


import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String src;
    private String title;
    private String alt;
    @ManyToOne
    @JoinColumn(name = "id_product")
    private Product product;

    public Image() {
    }

    public Image(String src, String title, String alt) {
        this.src = src;
        this.title = title;
        this.alt = alt;
    }

    public Image(Integer id, String src, String title, String alt) {
        this.id = id;
        this.src = src;
        this.title = title;
        this.alt = alt;
    }
    
    public Image(Integer id, String src, String title, String alt, Product product) {
        this.id = id;
        this.src = src;
        this.title = title;
        this.alt = alt;
        this.product = product;
    }

    public Image(String src, String title, String alt, Product product) {
        this.src = src;
        this.title = title;
        this.alt = alt;
        this.product = product;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
    
}

