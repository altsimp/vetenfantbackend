package vetenfant.backend.vetenfant.entity;

import java.util.ArrayList;
import java.util.List;


import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import vetenfant.backend.vetenfant.enums.GenderEnum;
import vetenfant.backend.vetenfant.enums.SeasonEnum;
import vetenfant.backend.vetenfant.enums.SizeEnum;
import vetenfant.backend.vetenfant.enums.StateEnum;


public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private GenderEnum gender;
    private Double price;
    private Integer size;
    private String description;
    private boolean isSold;
    @ManyToOne
    private Category category;
    @ManyToOne(fetch = FetchType.EAGER)
    private User seller;
    private SeasonEnum season;
    private StateEnum state;
    @OneToMany(mappedBy = "product")
    private List<Message> messages;
    private List<Image> images;

    public Product() {
    }
    public Product(String name, GenderEnum gender, Double price, Integer size, String description, boolean isSold,Category category,User user, SeasonEnum season, StateEnum state) {
        this.name = name;
        this.gender = gender;
        this.price = price;
        this.size = size;
        this.description = description;
        this.isSold = isSold;
        this.category = category;
        this.seller= user;
        this.season = season;
        this.state = state;
    }
    public Product(Integer id, String name, GenderEnum gender, Double price, Integer size, String description,
            boolean isSold,Category category,User user,SeasonEnum season, StateEnum state) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.price = price;
        this.size = size;
        this.description = description;
        this.isSold = isSold;
        this.category = category;
        this.seller = user;
        this.season = season;
        this.state = state;
    }
    public Product(Integer id, String name, GenderEnum gender, Double price, Integer size, String description,
            boolean isSold,Category category,SeasonEnum season, StateEnum state) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.price = price;
        this.size = size;
        this.description = description;
        this.isSold = isSold;
        this.category = category;
        this.season = season;
        this.state = state;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public GenderEnum getGender() {
        return gender;
    }
    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Integer getSize() {
        return size;
    }
    public void setSize(Integer size) {
        this.size = size;
    }    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public boolean getSold() {
        return isSold;
    }
    public void setSold(boolean isSold) {
        this.isSold = isSold;
    }
    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }
    public User getSeller() {
        return seller;
    }
    public void setSeller(User seller) {
        this.seller = seller;
    }
    public SeasonEnum getSeason() {
        return season;
    }
    public void setSeason(SeasonEnum season) {
        this.season = season;
    }
    public void setState(StateEnum state) {
        this.state = state;
    }
    public StateEnum getState() {
        return state;
    }
    public List<Image> getImages() {
        return images;
    }
    public void setImages(List<Image> images) {
        this.images = images;
    }
    public void addImage(Image image) {
        this.images.add(image);
    }
    public Product orElseThrow(Object object) {
        return null;
    }
}
