package vetenfant.backend.vetenfant.entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;

public class Adress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String postalCode;
    @NotBlank
    private String city;
    @NotBlank
    private String street;
    @NotBlank
    @ManyToOne
    @JoinColumn(name = "id_user")
    private Integer userId;
    

    public Adress(String postalCode, String city, String street) {
        this.postalCode = postalCode;
        this.city = city;
        this.street = street;
    }

    public Adress(Integer id, String postalCode, String city, String street) {
        this.id = id;
        this.postalCode = postalCode;
        this.city = city;
        this.street = street;
    }    

    public Adress(String postalCode, String city, String street, Integer userId) {
        this.postalCode = postalCode;
        this.city = city;
        this.street = street;
        this.userId = userId;
    }

    public Adress(Integer id, String postalCode, String city, String street,Integer userId) {
        this.id = id;
        this.postalCode = postalCode;
        this.city = city;
        this.street = street;
        this.userId=userId;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }  
    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    } 
}
