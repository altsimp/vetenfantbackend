package vetenfant.backend.vetenfant.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.boot.autoconfigure.amqp.RabbitConnectionDetails.Address;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

import jakarta.validation.constraints.NotBlank;


public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private String accountName;
    @NotBlank
    private String email;
    @NotBlank
    private String password;
    private String phoneNumber;
    private String name;
    private String firstName;
    private String description;
    private String profileImage;
    private Double evaluation;
    private String role;
    @OneToMany(mappedBy = "user")
    private List<Adress> adresses; 
    @OneToMany(mappedBy = "user")
    private List<Claim> claims;
    @OneToMany(mappedBy = "user")
    private List<Product> sellingProducts;
    @OneToMany(mappedBy = "receiver")
    private List<Message> receivedMessages;
    @OneToMany(mappedBy = "sender")
    private List<Message> sentMessages;
    private Image image;


    public User() {
    }

    public User(String accountName, @NotBlank String email, @NotBlank String password, String phoneNumber,
            String name, String firstName, String description, Double evaluation,String role) {
        this.accountName = accountName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.firstName = firstName;
        this.description = description;
        this.evaluation = evaluation;
        this.role="ROLE_USER";
    }

    public User(Integer id,String accountName, @NotBlank String email,String name, String firstName,String role) {
        this.id = id;
        this.accountName = accountName;
        this.email = email;
        this.role =role;
        this.name = name;
        this.firstName = firstName;
    }
        public User(Integer id, String accountName, String email,String phoneNumber, String name,String firstName, String description, Double evaluation,String role) {
        this.id = id;
        this.accountName = accountName;
        this.email = email;
        this.phoneNumber =phoneNumber;
        this.name = name;
        this.firstName = firstName;
        this.description = description;
        this.evaluation = evaluation;
        this.role="ROLE_USER";
    }
    public User(Integer id, String accountName, String email, String password,String phoneNumber, String name,String firstName, String description, Double evaluation,String role) {
        this.id = id;
        this.accountName = accountName;
        this.email = email;
        this.password = password;
        this.phoneNumber =phoneNumber;
        this.name = name;
        this.firstName = firstName;
        this.description = description;
        this.evaluation = evaluation;
        this.role="ROLE_USER";
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Double evaluation) {
        this.evaluation = evaluation;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Adress> getAdresses() {
        return adresses;
    }
    public void setAdresses(List<Adress> adresses) {
        this.adresses = adresses;
    }


    public List<Claim> getClaims() {
        return claims;
    }

    public void setClaims(List<Claim> claims) {
        this.claims = claims;
    }

    public List<Product> getSellingProducts() {
        return sellingProducts;
    }

    public void setSellingProducts(List<Product> products) {
        this.sellingProducts = products;
    }

    public List<Message> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(List<Message> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }

    public List<Message> getSentMessages() {
        return sentMessages;
    }

    public void setSentMessages(List<Message> sentMessages) {
        this.sentMessages = sentMessages;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        /* return List.of(
                new SimpleGrantedAuthority(role)); */
                return null;
    }

/*     @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
    return Collections.singletonList(new SimpleGrantedAuthority(role)); */


    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
