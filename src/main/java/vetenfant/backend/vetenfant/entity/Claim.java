package vetenfant.backend.vetenfant.entity;

import jakarta.persistence.Entity;
import java.time.LocalDate;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import vetenfant.backend.vetenfant.enums.StatusEnum;


public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    private LocalDate date;
    private Boolean status;
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;
    
    public Claim() {
    }

    public Claim(String title, String description, LocalDate date, Boolean status) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = status;
    }
    public Claim(Integer id,String title, String description, LocalDate date, Boolean status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = status;
    }
    public Claim(String title, String description, LocalDate date, Boolean status, User user) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = status;
        this.user = user;
    }
    public Claim(Integer id, String title, String description, LocalDate date, Boolean status, User user) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = status;
        this.user = user;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
