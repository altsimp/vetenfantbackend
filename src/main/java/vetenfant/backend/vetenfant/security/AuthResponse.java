package vetenfant.backend.vetenfant.security;

import org.springframework.security.core.userdetails.UserDetails;

public class AuthResponse {
    private UserDetails user; 
    private String token; 

    public AuthResponse(UserDetails user, String token){
        this.user = user; 
        this.token = token;
    }

    public UserDetails getUser() {
        return user;
    }

    public void setUser(UserDetails user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

