package vetenfant.backend.vetenfant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import vetenfant.backend.vetenfant.entity.User;


@Component
@Transactional
public class DataLoader implements ApplicationRunner {
    @Autowired
    private EntityManager em;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // créer un nouvel entité user
        User user = new User();
        user.setEmail("test@test.com");
        user.setAccountName("username");
        
        // hasher le password 
        user.setPassword(passwordEncoder.encode("1234"));

        // rôle 
        user.setRole("ROLE_USER");

        //em.persist(user);
    }
}

